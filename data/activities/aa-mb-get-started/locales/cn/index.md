### 连接 micro:bit

**微块新手？** 请参阅 [开始](https://microblocks.fun/get-started).

打开 MicroBlocks 并使用 USB 数据线将 micro:bit 连接到您的计算机。

如果在浏览器中运行 MicroBlocks，请单击 USB 图标进行连接。
MicroBlocks 桌面应用程序将自动连接到 micro:bit。

将出现一个绿色圆圈，表明 micro:bit 已连接。

![](connectionCircle.png =150x*)

### 纽扣

从 *Control* 中，将两个 **when button pressed** 块拖入脚本区域。

![](controlCategory.png =140x*)

使用菜单将其中一个中的 A 更改为 B。

![](buttonAHat.png) ![](buttonBHat.png)

选择 LED 显示屏库。

![](LEDDisplayLibrary.png =160x*)

使用这些脚本使 micro:bit 上的按钮打开和关闭 LED 显示屏：

![](buttonAFace.png) ![](buttonBClear.png)

通过单击小框来编辑显示块。单击一个框并用鼠标向下拖动以更改多个框。

### 动画

使用显示、等待和重复块创建字母、数字、形状和符号的动画。你能说出你的名字吗？

![](animation-hey.png) ![](animation-face.png) ![](animation-shapes.png)

使用按钮帽来启动动画。如何控制动画速度？

### 滚动文本

将`滚动文本`块拖到脚本区域，然后单击文本进行自定义。

![](scrollText.png)

您还可以滚动数字。试试这个脚本来创建倒计时！

![](scroll321Go.png)
